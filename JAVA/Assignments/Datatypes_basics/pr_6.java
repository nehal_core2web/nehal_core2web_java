class Calendar{
	public static void main(String[] args)	{
		int date= 20;
		String month="June";
		int year=2024;
		long seconds_day=86400;
		long seconds_month=2628288;
		long seconds_year=31536000;
		System.out.println("Date: "+date);
		System.out.println("Month: "+month);
		System.out.println("Year: "+year);
	        System.out.println("Total no of seconds in a day: "+seconds_day);
		System.out.println("Total no of seconds in a month: "+seconds_month);
	        System.out.println("Total no of seconds in a year: "+seconds_year);
	}
}
