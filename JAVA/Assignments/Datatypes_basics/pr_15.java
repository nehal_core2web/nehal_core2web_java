class Shopping {
    public static void main(String[] args) {
        int orderId = 123;
        double price = 500;
        boolean isAvailable = true;
        char size = 'L';
        String custName = "Punam Patil";
        System.out.println("Order Details:");
        System.out.println("Order ID: " + orderId);
        System.out.println("Customer Name: " + custName);
        System.out.println("Item Price: " + price);
        System.out.println("Item Size: " + size);
        System.out.println("Item Available: " + isAvailable);
    }
}
