class Quantity {
    public static void main(String[] args) {
        double litre = 3.5;
        double gram = 1500.0;
        System.out.println("The quantity of liters is: " + litre + " L");
        System.out.println("The weight in grams is: " + gram + " g");
    }
}
