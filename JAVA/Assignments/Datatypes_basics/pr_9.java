class Temperature {
    public static void main(String[] args) {
        double acTemp = 25.5;
        double stRoomTemp = 25.0;
        System.out.println("AC Temperature: " + acTemp + "°C");
        System.out.println("Standard Room Temperature: " + stRoomTemp + "°C");
    }
}
