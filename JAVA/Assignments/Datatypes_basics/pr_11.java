class Gravity {
    public static void main(String[] args) {
        double gravity_val = 9.8;
        char gravityLetter = 'g';
        System.out.println("Value of gravity is: " + gravity_val);
        System.out.println("The letter used to represent the acceleration due to gravity is: " + gravityLetter);
    }
}
